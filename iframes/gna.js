// gna.js
//------------------------------
//
// 2015-03-31, Jonas Colmsjö
//
//------------------------------
//
// Class that handles pages and navigation for small apps called varps.
//
//
// Using Google JavaScript Style Guide:
// http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------

if (!inBrowser) {
  var Helpers = require('./helpers.js');
  var Mousetrap = require('../bower_components/mousetrap/mousetrap.js');
}

// constructor
var Gna = function(options) {
  var self = this;
  var loggerOptions = {
    debug: true,
    filename: 'gna.js',
    noLogging: false
  };

  self.logger_ = new Helpers.log(loggerOptions);

  self.logger_.debug('Constructor.');

  self.pages_ = {};
  self.varps_ = {};
  self.currentPageId_ = null;
  self.pageEventHandler_ = null;

  if (options !== undefined && options !== null) {
    this.showErrors(options.showErrors);
  }

};

// Some constants
// --------------

// A page can have these states
// ----------------------------
// ```
//              +-------------------+
//              |                   v
// LOADED -> VISIBLE -> HIDDEN -> STOPPED
//              ^          |
//              +----------+
// ```

// A page is stopped and cannot be showed again
Gna.STOPPED = -1;
// The page is hidden
Gna.HIDDEN = 0;
// A page has this state when it has been loaded but never showed
Gna.LOADED = 1;
// The page is currently showed
Gna.VISIBLE = 2;

// Generic functions
// -----------------

Gna.prototype.showErrors = function(on) {
  var self = this;
  self.logger_.debug('showErrors:' + on);

  var alertFunc = function(errorMsg, url, lineNumber, column, errorObj) {
    var msg = '';
    msg += (errorMsg) ? errorMsg : '';
    msg += (url) ? '\n' + url : '';
    msg += (lineNumber) ? ':' + lineNumber : '';
    msg += (column) ? ':' + column : '';
    msg += (errorObj) ? '\n' + errorObj : '';

    msg += "\n\nCheck the browser's javascript console for more details.";

    alert(msg);
  };

  window.onerror = (on) ? alertFunc : null;

  return (on) ? 'Errors are showed.' : 'Errors are not showed.';
};

// This will create a script tag with the code in `data`
// `head` makes it possible to load scripts within `iframes`
Gna.prototype.loadScript = function(data, id, head) {
  var self = this;
  self.logger_.debug('loadScript:' + id);

  // generated id for script
  id = (id !== undefined && id !== null) ? '$$' + id + '$$' : null;

  // check if script is loaded
  if (id !== undefined && document.getElementById(id) !== null) {
    return;
  }

  var head = head || document.head || document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.defer = true;
  script.async = false;
  script.text = data;

  if (id !== null) {
    script.id = id;
  }

  head.appendChild(script);
};

// remove all current stylesheets and load a new. Call without argument
// to just delete all current stylesheets
// `document` makes it possbile
Gna.prototype.loadStylesheet = function(cssData, document) {
  var self = this;
  self.logger_.debug('loadStylesheet');

  var head = document.head || document.getElementsByTagName('head')[0];

  // remove the current stylesheets
  while (document.getElementsByTagName('style').length > 0) {
    head.removeChild(document.getElementsByTagName('style')[0]);
  }

  // create the new stylesheet
  if (cssData !== undefined && cssData !== null) {

    var style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet) {
      style.styleSheet.cssText = cssData;
    } else {
      style.appendChild(document.createTextNode(cssData));
    }

    head.appendChild(style);
  }
};

var handleEvent = function(evt) {
  if (typeof self.pageEventHandler_ !== 'undefined') {
    self.pageEventHandler_(evt);
  }
};

// supported events
window.addEventListener('input', handleEvent, false);
window.addEventListener('focusout', handleEvent, false);
window.addEventListener('click', handleEvent, false);

// Functions that manages varps
// ---------------------------------------------------------------------------
//

Gna.prototype.loadVarp = function(varp) {
  var self = this;
  var varpDef = varp.varpDef;

  self.logger_.debug('load varp:' + varpDef.id +
                     ' with permissions:' + varpDef.permissions);

  // check mandatory input
  if (varpDef.id === undefined ||
    varpDef.html === undefined ||
    varpDef.eventHandler === undefined ||
    varpDef.permissions === undefined) {

    throw "Invalid varp definition";
  }

  self.varps_[varpDef.id] = varp;

  varpDef.state = Gna.LOADED;

  // create shortcut
  if (varpDef.shortcut) {
    Mousetrap.bind(varpDef.shortcut, function() {
      self.showVarp(varpDef.id);
    }.bind(self));
  }

  // create iframe and add to the document
  var iframe = document.createElement('iframe');
  iframe.id = varpDef.id;

  // It will not be possible to send messages to the top window, nor possbile
  // to access the parent attribute unless `allow-same-origin` is set.
  // 'allow-scripts allow-forms' should always be set
  iframe.sandbox = varpDef.permissions;

  // Add iframe to varps element
  document.getElementById('varps').appendChild(iframe);

  iframe.style.width = '100%';
  iframe.style.height = '100%';
  iframe.style.border = 0;
  iframe.hidden = true;

  // Add html to body of iframe
  iframe.contentWindow.document.body.innerHTML = varpDef.html;

  // Add stylesheet to iframe
  loadStyleSheet(varp.style, iframe.contentWindow.document);

};

Gna.prototype.showVarp = function(iframeId) {
  var vs = document.getElementById('varps');
  var v = document.getElementById(iframeId);

  // check that the iframe exists
  if (v === null) {
    return;
  }

  // hide all varps
  for(var i=0; i<vs.childNodes.length; i++) {
    vs.childNodes[i].hidden = true;
  }

  // show the varp
  v.hidden = false;
};

// Functions that manage pages withn varps
// ---------------------------------------------------------------------------
//

Gna.prototype.load = function(page) {
  var self = this;
  var pageDef = page.pageDef;

  self.logger_.debug('load page:' + pageDef.id);

  // check mandatory input
  if (pageDef.id === undefined ||
    pageDef.html === undefined ||
    pageDef.eventHandler === undefined) {

    throw "Invalid page definition";
  }

  self.pages_[pageDef.id] = page;

  pageDef.state = Gna.LOADED;

  pageDef.element = document.createElement('body');
  pageDef.element.id = pageDef.id;
  pageDef.element.innerHTML = pageDef.html;

  // create shortcut
  if (pageDef.shortcut) {
    Mousetrap.bind(pageDef.shortcut, function() {
      self.show(pageDef.id);
    }.bind(self));
  }
};

Gna.prototype.show = function(pageId) {
  var self = this;
  self.logger_.debug('show:' + pageId);

  var head = document.head || document.getElementsByTagName('head')[0];

  var page = self.pages_[pageId];
  var pageDef = page.pageDef;

  if (pageDef.state === Gna.STOPPED) {
    throw "Can't show a page that is stopped: " + pageId;
  }

  var prevPageId = self.currentPageId_;
  self.currentPageId_ = pageId;

  // Hide the previous page
  self.hide(prevPageId);

  // create the page if it does not exist
  if (pageDef.element === undefined) {
    createPage(pageDef);
  }

  // set the new event handler
  self.pageEventHandler_ = pageDef.eventHandler;

  // show the new page
  document.body = pageDef.element;

  // set the title to the page description
  if (pageDef.description !== undefined) {
    document.title = pageDef.description;
  }

  // add shortcut to description
  if (pageDef.shortcut !== undefined) {
    document.title += ' (' + pageDef.shortcut + ')';
  }

  // remove the current stylesheets
  while (document.getElementsByTagName('style').length > 0) {
    head.removeChild(document.getElementsByTagName('style')[0]);
  }

  // create the new stylesheet
  if (pageDef.style !== undefined &&
    pageDef.style !== null) {

    var css = pageDef.style;
    var style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
  }

  // initialize the page if needed (only performed once)
  if (pageDef.state === Gna.LOADED) {
    self.init(pageId);
  } else {
    page.show();
  }

};

Gna.prototype.init = function(pageId) {
  var self = this;
  self.logger_.debug('init:' + pageId);

  // Make sure the page exists
  if (pageId === null ||
    (pageId !== null && self.pages_[pageId] === undefined)) {
    return;
  }

  var page = self.pages_[pageId];
  var pageDef = page.pageDef;

  if (page.init !== undefined && page.init !== null) {
    page.init();
  }

  pageDef.state = Gna.VISIBLE;

};

Gna.prototype.hide = function(pageId) {
  var self = this;
  self.logger_.debug('hide:' + pageId);

  // Make sure the page exists
  if (pageId === null ||
    (pageId !== null && self.pages_[pageId] === undefined)) {
    return;
  }

  var page = self.pages_[pageId];
  var pageDef = page.pageDef;

  if (page.hide !== undefined && page.hide !== null) {
    page.hide();
  }

  pageDef.state = Gna.HIDDEN;

};

Gna.prototype.stop = function(pageId) {
  var self = this;
  self.logger_.debug('stop:' + pageId);

  // Make sure the page exists
  if (pageId === null ||
    (pageId !== null && self.pages_[pageId] === undefined)) {
    return;
  }

  var page = self.pages_[pageId];
  var pageDef = page.pageDef;

  if (page.stop !== undefined && page.stop !== null) {
    page.stop();
  }

  pageDef.state = Gna.STOPPED;

};

// list the loaded pages
Gna.prototype.list = function() {
  var self = this;
  self.logger_.debug('list');

  var keys = Object.keys(this.pages_);

  var res = [];

  for (i = 0; i < keys.length; i++) {
    res.push(keys[i] + ((this.pages_[keys[i]].shortcut !== undefined) ? ' (' +
    this.pages_[keys[i]].shortcut + ')' : ''));
  }

  return res;
};

// Functions that works on templates embedded in `<script>` tags
// ---------------------------------------------------------------------------
//

Gna.prototype.loadTemplate = function(htmlId, scriptId, cssId) {
  var self = this;

  if (htmlId === undefined || htmlId === null) {
    throw 'Error in Gna.load! Mandatory attribute htmlId missing.';
  }

  // Initialize some attributes
  self.body = (self.body === undefined) ? {} : self.body;
  self.script = (self.script === undefined) ? {} : self.script;
  self.css = (self.css === undefined) ? {} : self.css;

  var html = document.getElementById(htmlId).innerHTML;

  self.body[htmlId] = document.createElement('body');
  self.body[htmlId].innerHTML = html;

  if (scriptId !== undefined && scriptId !== null) {
    self.script[htmlId] = document.getElementById(scriptId).innerHTML;
  }

  if (cssId !== undefined && cssId !== null) {
    self.css[htmlId] = document.getElementById(cssId).innerHTML;
  }

};

Gna.prototype.showTemplate = function(htmlId) {
  var self = this;

  if (htmlId === undefined || htmlId === null) {
    throw 'Error in Gna.show! Mandatory attribute htmlId missing.';
  }

  document.body = self.body[htmlId];

  if (self.script[htmlId] !== undefined) {
    self.loadScript(self.script[htmlId], htmlId);
  }

  // switch stylesheet (or just delete the existing)
  self.loadStylesheet(self.css[htmlId]);

};

//
// Manage modules
// ---------------------------------------------------------------------------
// This is a small hack to make it possbile to use the module both with and
// without browserify

// The name of the module becomes the filename automatically in browserify
module.exports = Gna;

// export the module without browserify
if (inBrowser) {
  window['Gna'] = module.exports;
}
