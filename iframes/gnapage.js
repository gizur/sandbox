// Base class for pages
// ---------------------------------------------------------------------------
//

if (!inBrowser) {
  var Helpers = require('./helpers.js');
}

// constructor
var GnaPage = function() {
  var self = this;
  var loggerOptions = {
    debug: true,
    filename: 'GnaPage.js',
    noLogging: false
  };

  self.logger_ = new Helpers.log(loggerOptions);
  self.logger_.debug('Constructor.');
};

GnaPage.prototype.init = function() {
  var self = this;
  self.logger_.debug('init.');
};

GnaPage.prototype.show = function(evt) {
  var self = this;
  self.logger_.debug('show.');
};

GnaPage.prototype.hide = function(evt) {
  var self = this;
  self.logger_.debug('hide.');
};

GnaPage.prototype.stop = function(evt) {
  var self = this;
  self.logger_.debug('stop.');
};

GnaPage.prototype.evtHandler = function(evt) {
  return;
};

GnaPage.prototype.pageDef = {
  id: null,
  description: null,
  html: null,

  eventHandler: null,
  shortcut: null,
  style:null
};

//
// Manage modules
// ---------------------------------------------------------------------------
// This is a small hack to make it possbile to use the module both with and
// without browserify

// The name of the module becomes the filename automatically in browserify
module.exports = GnaPage;

// export the module without browserify
if (inBrowser) {
  window['GnaPage'] = module.exports;
}
