#!/usr/bin/env node

// parseHTML.js
//
// 150408, Jonas COlsjö
//
//
// Usage: npm install minimist; npm install chai
//        ./parseHTML.js -f <file> -t <tag> | --test

var argv = require('minimist')(process.argv.slice(2));
var assert = require('chai').assert;
var fs = require('fs');

var parse = function(startStr, endStr, str) {
  var exp = startStr + '(.*)' + endStr;
  var re = new RegExp(exp);
  var noBreaks = str.replace(/(\r\n|\n|\r)/gm, '');
  var res = re.exec(noBreaks);

  return (res) ? res[1] : null;

};

var parseTag = function(tag, str) {
  return parse('<' + tag + '>', '</' + tag + '>', str);

  var exp = '(.*)';
  var re = new RegExp(exp);
  var noBreaks = str.replace(/(\r\n|\n|\r)/gm, '');
  var res = re.exec(noBreaks);

  return (res) ? res[1] : null;
};

var removeHTMLComments = function(str) {
  if (!str || str.length === 0) return '';

  var start = str.indexOf('<!--');
  var end = str.indexOf('-->');

  if (start === -1 || end === -1) return str;

  var res = str.substring(0, start);
  str = str.substring(end + 3, str.length);

  var res2 = removeHTMLComments(str);
  return res.concat(res2);
};

// Inner without attributes in tags
var parseTag2 = function(tag, str) {
  if (!str || str.length === 0) return [];

  var start = str.indexOf('<' + tag + '>');
  var end = str.indexOf('</' + tag + '>');
  var tagLength = ('<' + tag + '>').length;

  if (start === -1 || end === -1) return [];

  var res = str.substring(start + tagLength, end);
  str = str.substring(end + tagLength + 1, str.length);

  var res2 = parseTag2(tag, str);
  return [res].concat(res2);

};

// Outer with attributes in tag
var parseTag3 = function(tag, str) {
  if (!str || str.length === 0) return [];

  // find start of beginning and end tags
  var startStartTag = str.indexOf('<' + tag);
  var startEndTag = str.indexOf('</' + tag + '>');
  if (startStartTag === -1 || startEndTag === -1) return [];

  // check that the tag has an end
  var endStartTag = str.substring(startStartTag + tag.length, str.length).indexOf('>');
  if(endStartTag === -1) return [];

  var endTagLength = ('</' + tag + '>').length;

  var res = str.substring(startStartTag , startEndTag + endTagLength);
  str = str.substring(startEndTag + endTagLength, str.length);

  var res2 = parseTag3(tag, str);
  return [res].concat(res2);

};

// Outer and inner with attributes in tag
var parseTag4 = function(tag, str, outer) {
  if (!str || str.length === 0) return [];

  // find start of beginning and end tags
  var startStartTag = str.indexOf('<' + tag);
  var startEndTag = str.indexOf('</' + tag + '>');
  if (startStartTag === -1 || startEndTag === -1) return [];

  // check that the tag has an end
  var endStartTag = str.substring(startStartTag + tag.length, str.length).indexOf('>');
  if(endStartTag === -1) return [];
  endStartTag += startStartTag + tag.length;

  var startTagLength = endStartTag - startStartTag;
  var endTagLength = ('</' + tag + '>').length;

  var res;
  res = (outer) ? str.substring(startStartTag , startEndTag + endTagLength) :
                  str.substring(startStartTag + startTagLength + 1, startEndTag);

  str = str.substring(startEndTag + endTagLength, str.length);

  var res2 = parseTag4(tag, str, outer);
  return [res].concat(res2);

};

// Outer and inner with attributes in tag including the attributes
// [{outer:..., inner:..., attr:...}]
var parseTag5 = function(tag, str) {
  if (!str || str.length === 0) return [];

  // find start of beginning and end tags
  var startStartTag = str.indexOf('<' + tag);
  var startEndTag = str.indexOf('</' + tag + '>');
  if (startStartTag === -1 || startEndTag === -1) return [];

  // check that the tag has an end
  var endStartTag = str.substring(startStartTag + tag.length, str.length).indexOf('>');
  if(endStartTag === -1) return [];
  endStartTag += startStartTag + tag.length;

  var startTagLength = endStartTag - startStartTag;
  var endTagLength = ('</' + tag + '>').length;

  var res = {};
  res.outer = str.substring(startStartTag , startEndTag + endTagLength);
  res.inner = str.substring(startStartTag + startTagLength + 1, startEndTag);
  res.attr = str.substring(startStartTag + ('<' + tag).length, endStartTag);

  str = str.substring(startEndTag + endTagLength, str.length);

  var res2 = parseTag5(tag, str);
  return [res].concat(res2);

};

// Run unit tests
if (argv['test']) {

  // parseTag2
  // ----------------------

  assert.deepEqual(parseTag2('p', '<a>hej hej</a>'),
  [],
  'parseTag2: Zero matches failed');

  assert.deepEqual(parseTag2('p', '<p>hej hej</p>'),
  ['hej hej'],
  'parseTag2: Simple single matches failed');

  assert.deepEqual(parseTag2('p', 'bal bla<p>hej hej</p>då då'),
  ['hej hej'],
  'parseTag2: Single matches failed');

  assert.deepEqual(parseTag2('p', 'bal bla<p>hej hej</p><p>då då</p>'),
  ['hej hej', 'då då'],
  'parseTag2: Multiple matches failed');

  // removeHTMLComments
  // ----------------------

  assert.equal(removeHTMLComments('bal bla<!-- hej hej --><p>då då</p>'),
  'bal bla<p>då då</p>',
  'removeHTMLComments: simple test failed');

  assert.equal(removeHTMLComments('bal bla<!-- hej hej --><p>då då</p><!-- \n<script> -->oj oj'),
  'bal bla<p>då då</p>oj oj',
  'removeHTMLComments: simple test failed');

  // parseTag3
  // ----------------------

  assert.deepEqual(parseTag3('s', 'bal bla<s attr>hej hej</s><a>då då</a>'),
  ['<s attr>hej hej</s>'],
  'parseTag3: Single match failed');

  assert.deepEqual(parseTag3('s', 'bal bla<s attr>hej hej</s><s>då då</s>'),
  ['<s attr>hej hej</s>', '<s>då då</s>'],
  'parseTag3: Multiple matches failed');

  // parseTag4
  // ----------------------

  assert.deepEqual(parseTag4('s', 'bal bla<s attr>hej hej</s><a>då då</a>', true),
  ['<s attr>hej hej</s>'],
  'parseTag4: Single outer match failed');

  assert.deepEqual(parseTag4('s', 'bal bla<s attr>hej hej</s><s>då då</s>', true),
  ['<s attr>hej hej</s>', '<s>då då</s>'],
  'parseTag4: Multiple outer matches failed');

  assert.deepEqual(parseTag4('s', 'bal bla<s attr>hej hej</s><a>då då</a>', false),
  ['hej hej'],
  'parseTag4: Single inner match failed');

  assert.deepEqual(parseTag4('s', 'bal bla<s attr>hej hej</s><s>då då</s>', false),
  ['hej hej', 'då då'],
  'parseTag4: Multiple inner matches failed');


  // parseTag5
  // ----------------------

  assert.deepEqual(parseTag5('s', 'bal bla<s attr>hej hej</s><a>då då</a>', true),
  [{outer: '<s attr>hej hej</s>', inner: 'hej hej', attr: ' attr'}],
  'parseTag5: Single match failed');

  assert.deepEqual(parseTag5('s', 'bal bla<s attr>hej hej</s><s>då då</s>', true),
  [{outer: '<s attr>hej hej</s>', inner: 'hej hej', attr: ' attr'},
   {outer: '<s>då då</s>', inner: 'då då', attr: ''}],
  'parseTag5: Multiple matches failed');

  console.log('TEST SUCCESSFUL!!')

} else {

  fs.readFile(argv['f'], function(err, data) {
    if (err) throw err;

    data = removeHTMLComments(''+data);

    var res = parseTag(argv['t'], data);
    var res2 = parseTag2(argv['t'], data, 0);

    console.log('-- file: ' + argv['f'] + ' tag: ' + argv['t'] + ' --');
    console.log('\nresult parseTag (regexp without balancing groups):\n' + res);
    console.log('\nresult parseTag2 (using simple indexOf):\n' + res2.join('\n'));
  });

}
