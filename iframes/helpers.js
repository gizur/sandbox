// helpers.js
//------------------------------
//
// 2015-04-01, Jonas Colmsjö
//------------------------------
//
// Misc helpers fucntions
//
// Usage:
//
//   <script src='./src/helpers.js'></script>
//   ...
//   var loggerOptions = {
//     debug: true,
//     filename: 'myfile.js',
//     noLogging: false
//   };
//
//   var log = new Helpers.log(loggerOptions);
//
//
// Using Google JavaScript Style Guide:
// http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------

var Helpers = {};

Helpers.MODULE_NAME = 'Helpers';

// Logging class
// Each instance has its own options for logging level
//
// options: {
//   debug: boolean,
//   info: boolean,
//   noLogging: boolean,
//   filename: string to prefix logging with
// };

Helpers.log = function(options) {
  var self = this;
  self._debug = false;
  self._info = true;
  self._noLogging = false;
  self._filename = null;
  if (options !== undefined) {
    self.logLevel(options);
  }
};

Helpers.log.prototype.debug = function(text) {
  var self = this;
  if (self._debug && !self._noLogging) {
    self.log('DEBUG:' + text);
  }
};

Helpers.log.prototype.info = function(text) {
  var self = this;
  if (self._info && !self._noLogging) {
    self.log('INFO:' + text);
  }
};

Helpers.log.prototype.log = function(text) {
  var self = this;
  if (self._filename !== undefined && self._filename !== null) {
    text = self._filename + ':' + text;
  }

  if (!self._noLogging) {
    console.log(text);
  }
};

Helpers.log.prototype.logLevel = function(options) {
  var self = this;
  if (options.debug !== undefined) {
    self._debug = options.debug;
  }

  if (options.info !== undefined) {
    self._info = options.info;
  }

  if (options.noLogging !== undefined) {
    self._noLogging = options.noLogging;
  }

  if (options.filename !== undefined) {
    self._filename = options.filename;
  }
};

//
// Export module
// ---------------------------------------------------------------------------
// This is a small hack to make it possbile to use the module both with and
// without browserify
//
// traditional include:   <script src="./src/varp/term.js"></script>
// use `require('template.js')` in `main.js` when building with browserify:
// `browserify -d main.js -o bundle.js`

inBrowser = (typeof module === 'undefined');

if (inBrowser) {
  window.module = {
    exports: {}
  };
}

// Make require just return the Object with the required name in the browser
/* DOES NOT WORK! filename != module name
if (inBrowser && typeof require === 'undefined') {
  require = function(m) {
    if (window[m] === 'undefined') {
      throw 'Module: ' + m + ' not loaded! This needs to be done using the ' +
      'script tag when not using browserify.';
    }

    return window[m];
  }
}
*/

// The name of the module becomes the filename automatically in browserify
module.exports = Helpers;

// export the module without browserify
if (inBrowser) {
  window[Helpers.MODULE_NAME] = module.exports;
}
